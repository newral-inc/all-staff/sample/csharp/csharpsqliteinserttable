﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;

namespace CSharpSQLiteInsertTable
{
    class Program
    {
        static void Main(string[] args)
        {
            // テーブルを作成します。
            using (var connection = new SQLiteConnection("Data Source=Sample.db"))
            {
                connection.Open();
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "CREATE TABLE IF NOT EXISTS Sample( Name TEXT)";
                    command.ExecuteNonQuery();
                }
                connection.Close();
            }

            // データを新規登録します。
            using (var connection = new SQLiteConnection("Data Source=Sample.db"))
            {
                connection.Open();
                using (var transaction = connection.BeginTransaction())
                {
                    var command = connection.CreateCommand();
                    command.CommandText = $"INSERT INTO Sample (Name) VALUES (@Name)";
                    command.Parameters.Add("Name", System.Data.DbType.String);

                    command.Parameters["Name"].Value = "佐藤";
                    command.ExecuteNonQuery();

                    command.Parameters["Name"].Value = "鈴木";
                    command.ExecuteNonQuery();

                    command.Parameters["Name"].Value = "高橋";
                    command.ExecuteNonQuery();

                    transaction.Commit();
                }
                connection.Close();
            }
        }
    }
}
